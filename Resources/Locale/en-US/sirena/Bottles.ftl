ent-Artecalcet_sirena_ChemistryBottle = Artecalcet bottle
    .desc = Silver based drug. Used in all mind-altering drugs. In its pure form causes poisoning.
    .suffix = { "Siren" }

ent-Arteicog_sirena_ChemistryBottle = bottle of arteicog
    .desc = Artecalcet-based drug. When exposed to a reasonable person, it causes a sedative effect. In overdose it puts the target to sleep.
    .suffix = { "Siren" }

ent-Gadoiprinim_sirena_ChemistryBottle = Gadoiprinim bottle
    .desc = Artecalcet-based drug. Reduces pain when administered internally.
    .suffix = { "Siren" }

ent-Vinikefastatin_sirena_ChemistryBottle = vinikefastatin bottle
    .desc = Artecalcet-based drug. A drug that activates certain parts of the brain.
    .suffix = { "Siren" }

ent-Aphrodisiac_sirena_ChemistryBottle = aphrodisiac bottle
    .desc = Artecalcet-based drug. Stimulates sexual arousal.
    .suffix = { "Siren" }