ent-SpawnerERTLeader_sirena = ERT leader spawner
ent-SpawnerERTLeaderEVA_sirena = ERT leader spawner
    .suffix = EVA
ent-SpawnerERTJanitor_sirena = ERT janitor spawner
ent-SpawnerERTJanitorEVA_sirena = ERT janitor spawner
    .suffix = EVA
ent-SpawnerERTEngineer_sirena = ERT engineer spawner
ent-SpawnerERTEngineerEVA_sirena = ERT engineer spawner
    .suffix = EVA
ent-SpawnerERTSecurity_sirena = ERT security spawner
ent-SpawnerERTSecurityEVA_sirena = ERT security spawner
    .suffix = EVA
ent-SpawnerERTMedical_sirena = ERT medical spawner
ent-SpawnerERTMedicalEVA_sirena = ERT medical spawner
    .suffix = EVA
ent-SpawnerCBURNUnit_sirena = CBURN unit spawner
ent-SpawnerCentcomOfficial_sirena = centcom official spawner
ent-SpawnerSyndicateAgent_sirena = syndicate agent spawner
ent-SpawnerNukeOp_sirena = nuke op spawner