ent-BaseBorgModule = модуль борга
    .desc = Техническая деталь, дающая киборгам новые способности.

ent-BaseProviderBorgModule = { "" }
    .desc = { "" }

ent-BaseBorgModuleCargo = { ent-BaseBorgModule }
    .desc = { ent-BaseBorgModule.desc }
ent-BaseBorgModuleEngineering = { ent-BaseBorgModule }
    .desc = { ent-BaseBorgModule.desc }
ent-BaseBorgModuleJanitor = { ent-BaseBorgModule }
    .desc = { ent-BaseBorgModule.desc }
ent-BaseBorgModuleMedical = { ent-BaseBorgModule }
    .desc = { ent-BaseBorgModule.desc }
ent-BaseBorgModuleScience = { ent-BaseBorgModule }
    .desc = { ent-BaseBorgModule.desc }
ent-BaseBorgModuleService = { ent-BaseBorgModule }
    .desc = { ent-BaseBorgModule.desc }
ent-BaseBorgModuleSecurity = { ent-BaseBorgModule }
    .desc = { ent-BaseBorgModule.desc }
ent-BaseBorgModuleSyndicate = { ent-BaseBorgModule }
    .desc = { ent-BaseBorgModule.desc }

#Cargo
ent-MiningModuleCargoSirena = модуль добычи полезных ископаемых
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-AdvMiningModuleCargoSirena = продвинутый модуль добычи полезных ископаемых
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-DeliveryModuleCargoSirena = модуль перевозки грузов
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 

#Engineering
ent-AdvInstrumentalModuleEngineeringSirena = модуль продвинутых инструментов
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-CableModuleEngineeringSirena = модуль укладки кабелей
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-ResourceModuleEngineeringSirena = ресурсный модуль
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-AdvResourceModuleEngineeringSirena = продвинутый ресурсный модуль
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-RCDModuleEngineeringSirena = модуль РСУ
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 

#Generic
ent-ManipulatorModuleGenericSirena = модуль манипуляторов
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-BorgModuleTool = модуль базовых инструментов
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-AdvStorageModuleGenericSirena = модуль БС хранения
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-SearchModuleGenericSirena = поисковый модуль
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-JetpackModuleGenericSirena = модуль ПБП
    .desc = Техническая деталь, дающая киборгам новые способности. Модуль ПБП или же модуль перемещения в безвоздушном пространстве. Позволяет киборгам работать вне зависимости от условий гравитаций.
    .suffix = Модуль киборга 

#Janitor
ent-CleanerModuleJanitorSirena = модуль уборки
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-AdvCleanerModuleJanitorSirena = продвинутый модуль уборки
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 

#Medical
ent-BaseAidModuleMedicalSirena = модуль оказания медицинской помощи
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-AdvAidModuleMedicalSirena = продвинутый модуль оказания медицинской помощи
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-LiquidAidModuleMedicalSirena = жидкостный модуль оказания медицинской помощи
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-ReanimationModuleMedicalSirena = ренимационный модуль
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 

#Science
ent-ExplorationModuleScienceSirena = модуль ИОС
    .desc = Техническая деталь, дающая киборгам новые способности. Модуль ИОСили же модуль изучения окружающей среды. Позволяет киборгам изучать любые условия внешней среды.
    .suffix = Модуль киборга 
ent-AnomalyModuleScienceSirena = модуль исследования аномалий
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 

#Service
ent-HydroponicsModuleServiceSirena = модуль садоводства
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-MusicalModuleServiceSirena = музыкальный модуль
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-ClownModuleServiceSirena = клоунский модуль
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 
ent-BartenderModuleServiceSirena = модуль бармена
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга 

#Security
ent-NonLethalModuleSecuritySirena = модуль нелетального вооружения
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга
ent-CriminalisticModuleSecuritySirena = криминалистический модуль
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга

#Syndicate
ent-CR20ModuleSyndicateSirena = модуль CR-20
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга, Синдикат
ent-BaseAidModuleSyndicateSirena = модуль мед. помощи
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга, Синдикат
ent-EmagModuleSyndicateSirena = модуль ЕМАГ
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга, Синдикат
ent-EswordModuleSyndicateSirena = модуль ближнего боя
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга, Синдикат

# UIC
ent-UICStandartModuleUICSirena = стандартный модуль UIC
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга

# ERT
ent-DSModuleERTSirena = модуль эскадрона смерти
    .desc = { ent-BaseBorgModule.desc }
    .suffix = Модуль киборга
